# based on poky
require ${COREBASE}/meta-poky/conf/distro/poky.conf

DISTRO = "trs"
DISTRO_NAME = "TRS (Trusted Reference Stack)"
SDK_VENDOR = "-trssdk"

MAINTAINER = "Linaro TRS"


# Unique but reproducible build identifier using repo tool and
# checksum of all git subrepository names and their "git describe --tags --always --dirty"
# status. Note that dirty build is not marked as such, only checksums will include the
# "dirty" string if git repos have uncommitted changes.
BUILD_ID := "${@bb.process.run('repo forall -c \
        \' printf \"%s \" $REPO_PATH && \
           git describe --tags --always --dirty \
        \' \
    | sha256sum | awk \'{print $1}\' ', cwd='${TOPDIR}/..')[0][:10].strip()}"

# based on trs git repo tags and commits but
# includes also BUILD_ID which includes all git repos in a checksum
DISTRO_VERSION := "${@bb.process.run('git describe --tags --always --dirty', cwd='${TOPDIR}/../trs')[0].strip()} (build_id ${BUILD_ID})"

# enable baremetal ewaol things also for "trs" DISTRO
DISTROOVERRIDES =. "ewaol:"

TARGET_VENDOR = "-trs"

# systemd as the init system
INIT_MANAGER = "systemd"
DISTRO_FEATURES:append = " virtualization ipv6 seccomp k3s"

# rpm as the package management system
PACKAGE_CLASSES ?= "package_rpm"

PREFERRED_PROVIDER_virtual/runc = "runc-opencontainers"
PREFERRED_PROVIDER_virtual/docker = "docker-ce"

# enable selinux
DISTRO_FEATURES:append = " acl xattr pam selinux"
PACKAGECONFIG_pn-sudo = "pam-wheel"

# set permissive mode for now. We can change this to enforcing once we have the
# proper policy installed
#DEFAULT_ENFORCING = "enforcing"
DEFAULT_ENFORCING = "permissive"
PREFERRED_PROVIDER_virtual/refpolicy = "refpolicy-mls"

# enable selinux
DISTRO_FEATURES:append = " acl xattr pam selinux"
PACKAGECONFIG_pn-sudo = "pam-wheel"

DISTRO_FEATURES:append = " systemd"
VIRTUAL-RUNTIME_init_manager = "systemd"
DISTRO_FEATURES_BACKFILL_CONSIDERED = "sysvinit"
VIRTUAL-RUNTIME_initscripts = ""

# enable EFI
DISTRO_FEATURES:append = " efi"

# enable tpm2
DISTRO_FEATURES:append = " tpm2"

# enable udisks2 (for clevis)
DISTRO_FEATURES:append = " polkit"

DISTRO_FEATURES:append = " xen"

# reduce qemu binary packages etc
DISTRO_FEATURES:append = " vmsep"

DOM0_MEMORY_SIZE = "1024"
