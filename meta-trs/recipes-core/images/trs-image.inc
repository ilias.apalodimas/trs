# Copyright (c) 2023, Linaro Limited.
#
# SPDX-License-Identifier: MIT

inherit core-image image-buildinfo extrausers

# meta-virtualization/recipes-containers/k3s/README.md states that K3s requires
# 2GB of space in the rootfs to ensure containers can start
#
# OpenAD Kit Demo Docker image requires 8 GB
ROOTFS_EXTRA_SPACE ?= "10000000"

IMAGE_ROOTFS_EXTRA_SPACE:append = "${@ ' + ${ROOTFS_EXTRA_SPACE}' \
                                      if '${ROOTFS_EXTRA_SPACE}' \
                                      else ''}"

IMAGE_FEATURES += "\
    bash-completion-pkgs \
    debug-tweaks \
    package-management \
    ssh-server-openssh \
"

IMAGE_INSTALL += " \
    bash \
    bash-completion-extra \
    ca-certificates \
    docker-ce \
    k3s-server \
    kernel-module-xen-blkback \
    kernel-module-xen-gntalloc \
    kernel-module-xen-gntdev \
    kernel-module-xen-netback \
    kernel-modules \
    ltp \
    procps \
    soafee-test-suite \
    sudo \
    wget \
    xen-tools \
"

EXTRA_IMAGEDEPENDS += "xen"

EXTRA_USERS_PARAMS:prepend = " useradd -p '' test; \
                               useradd -p '' trs; \
                               useradd -p '' ewaol; \
                               groupadd trs; \
                               groupadd sudo; \
                               usermod -aG trs ewaol; \
                               usermod -aG sudo trs; \
                               usermod -aG sudo ewaol; \
                               usermod -aG docker trs; \
                               usermod -aG docker ewaol; \
                             "

