# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

FILESEXTRAPATHS:prepend:ewaol := "${THISDIR}/files:"

SRC_URI += " file://path.sh"

do_install:append() {
    install -d ${D}${sysconfdir}/profile.d

    # Others are given 'read' permission so that profile env vars are passed
    # through to other user accounts correctly
    install -m 0644 ${WORKDIR}/path.sh \
        ${D}${sysconfdir}/profile.d/path.sh
}
